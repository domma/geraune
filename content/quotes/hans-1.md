+++
title = "Tobias Hans"
source = "https://twitter.com/fdpbt/status/1374347117666443265"
date = 2021-03-31
+++


> Im Herzen Europas darf kein Mensch Angst um seine Sicherheit haben. Der
> Rückzug von Tareq Alaows macht mich persönlich sehr betroffen und sollte uns
> allen zu denken geben. Toleranz und Solidarität sind nicht verhandelbar - für
> Fremdenhass ist in Deutschland kein Platz.

via <a href="https://twitter.com/fdpbt/status/1374347117666443265" target="_top">Twitter</a>

* <i>Im Herzen Europas darf kein Mensch Angst um seine Sicherheit haben.</i><br/>
  Die Frau in der dunklen Seitenstraße, nachts auf dem nach Hause weg. Der
  Schwarze auf dem Volksfest, im Kreise der AfD Wähler. Der Flüchtling im
  Mittelmeer. Diese Menschen haben Angst. Sie dürfen es.

* <i>... und sollte uns allen zu denken geben.</i><br/>
  Uns? Uns CDU Mitgliedern? Uns Ministerpräsident:innen? Uns gibt es schon lange
  zu denken.

* <i>Toleranz und Solidarität sind nicht verhandelbar</i><br/>
  Gehaltserhöhung und früherer Feierabend sind nicht verhandelbar. Höhere Hartz
  IV Sätze sind nicht verhandelbar.

* <i>für Fremdenhass ist in Deutschland kein Platz.</i><br/>
  Angriffe auf Asylsuchende und ihre Unterkünfte: 10944
  (<a href="https://www.mut-gegen-rechte-gewalt.de/service/chronik-vorfaellel">Quelle</a>)
