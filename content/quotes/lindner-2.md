+++
title = "Christian Lindner"
source = "https://twitter.com/fdpbt/status/1374347117666443265"
date = 2021-03-18
+++


> Die wissenschaftliche Schlussfolgerung der EMA zu #AstraZeneca ist deutlich:
> Der #Impfstoff ist sicher und wirksam. Der Nutzen ist wesentlich höher als das
> Risiko. Unter diesen Bedingungen muss das #Impfen weitergehen. Dazu braucht es
> dringend bessere Aufklärung für Vertrauen!

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no
sea takimata sanctus est Lorem ipsum dolor sit amet.
