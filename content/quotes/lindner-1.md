+++
title = "Christian Lindner"
source = "https://twitter.com/fdpbt/status/1374347117666443265"
date = 2021-03-23
+++


> Der Beschluss ist ein erschütterndes Dokument der Planlosigkeit. Mit dem
> nächtlichen Verfahren setzt das Kanzleramt zudem die Akzeptanz aufs Spiel. Die
> Beratungsqualität würde besser, wenn man morgens beginnt und wenn Parlament und
> Länderkabinette einbezieht

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod
tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At
vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no
sea takimata sanctus est Lorem ipsum dolor sit amet.
